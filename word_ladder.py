#importing re
import re
#to sort the item with its respective distance from target word
def same(item, target):
  #to return the distance of word from target word
  return len([c for (c, t) in zip(item, target) if c == t])
#build function is to create a array of the pattern
def build(pattern, words, seen, list):
  #returns the word if it is not in the dictionary and the list
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]
#defining the function
def find(word, words, seen, target, path):
  #creating list to store the pattern
  list = []
  #To read words
  for i in range(len(word)):
    #reading the words if i<len(word) and sending the pattern to build
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    #if no words are found then it returns false
    return False
  #to sort the list from its distance from target word
  list = sorted([(same(w, target), w) for w in list], reverse=True)
 #to check the last but 1 word in the path
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        #adding the word to path
        path.append(item)
      return True
    #it will return to main block
    seen[item] = True
    #if the distance between word and target is more than then it takes the first word from list
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()
#to handle run time errors
try:
  #taking dictionary name from user
  fname = input("Enter the dictionary name")
  #to open file
  file = open(fname)
  #reading the file
  lines = file.readlines()
  #if their si line in the file
  while True:
    #taking start word
    start = input("Enter start word:")
    #creating array
    words = []
    #reading each line
    for line in lines:
      #seperating the words and storing in dictionary
      word = line.rstrip()
      #checking length of word is same as start word
      if len(word) == len(start):
        #appending words to list if same lenght
        words.append(word)
    #checking the word in in dictionary
    if start not in words:
      #printing if word is not found
      print("Word not found in dictionary")
      #terminating the program
      exit()
    target = input("Enter target word:")
    #to check length of start word & target word are same
    if len(start) != len(target):
      #printing length error
      print("Length of the words must me equal")
      #terminating the program
      exit()
    if target not in words:
      #printing if word is not found
      print("Word not found in dictionary")
      #terminating the program
      exit()
    break

  count = 0
  #creating an array to store path
  path = [start]
  #creating dictionary
  seen = {start : True}
  #calling the function
  if find(start, words, seen, target, path):
   #adding target word to path
    path.append(target)
    #printing the path & length of path
    print(len(path) - 1, path)
  else:
    print("No path found")
#if any runtime errore occurs
except:
  print(" Enter correct dictionary name with extension ")
